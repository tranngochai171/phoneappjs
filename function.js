const readline = require('readline-sync');
let Contact = require('./contact/Contact');
const numberValue = ['phoneNum']
const findContact = (nameFinding, contactList, mode) => {
  for(let i = 0; i < contactList.length; i++) {
    if(nameFinding === contactList[i].name) {
      if(mode === 'edit')
        return contactList[i]
      else 
        return i;
    }
  }
  return null;
}
const editProperty = (contact, propertyName) => {
  const str = `Please input ${propertyName} contact you want to edit: `
  if(readline.keyInYNStrict(`Do you want to edit ${propertyName}?`))
    contact[propertyName] = numberValue.indexOf(propertyName) === -1 ? readline.question(str) : parseInt(readline.question(str))
}
const commonFunction = {
  printMenuAndChoose: () => {
    console.log('Menu')
    console.log('1: Add new contact')
    console.log('2: Edit contact')
    console.log('3: Show list contact')
    console.log('4: Remove contact')
    return parseInt(readline.question('Please choose one option: '))
  },
  option_1: (contacts) => {
    name = readline.question('Please input the name of the new contact: ')
    phoneNum = parseInt(readline.question('Please input phone number: '))
    contacts.push(new Contact(name, phoneNum))
  },
  option_2_4: (contacts, mode) => {
    while(true) {
      let nameFinding = readline.question(`Please input the name contact you want to ${mode}: `)
      const contactFining = findContact(nameFinding, contacts)
      if(contactFining !== null) {
        if(mode === 'edit') {
          for(let item in contactFining)
            editProperty(contactFining, item)
        } else {
          console.log(`Remove contact with name is ${contacts[contactFining].name} successfully!`)
          contacts.splice(contactFining, 1);
        }
      } else {
        console.log('The contact you are looking for is not in our contact list!')
      }
      if(!readline.keyInYNStrict('Do you want find another contact?'))
        break;
    }
  },
  option_3: (contacts) => {
    console.log(`We have ${contacts.length} in contact list: `)
    console.log(contacts)
  }
}
module.exports = commonFunction;