let commonFunction = require('./function')
let fs = require('fs');
let readline = require('readline-sync');
let contacts = JSON.parse(fs.readFileSync('./document.txt'));

console.log('Welcome to Phone Management App')
while (true) {
  switch (commonFunction.printMenuAndChoose()) {
    case 1:
      commonFunction.option_1(contacts)
      fs.writeFileSync('./document.txt', JSON.stringify(contacts))
      break;
    case 2:
      commonFunction.option_2_4(contacts, 'edit')
      fs.writeFileSync('./document.txt', JSON.stringify(contacts))
      break;
    case 3:
      commonFunction.option_3(contacts)
      break;
    case 4:
      commonFunction.option_2_4(contacts, 'delete')
      fs.writeFileSync('./document.txt', JSON.stringify(contacts))
      break;
    default:
      console.log('Please choose again! (1-4)');
  }
}

